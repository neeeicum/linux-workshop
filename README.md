# Linux Workshop

This repository has the pdf version of the Linux Workshop - Light Introduction, lectured on  of September 2019

## Content

- Shell (Bash)
- Shell (Script)
- Filesystem
- Processes
- Software Management
- Editors

## Requirements

PC with Debian-based Linux Distribution

## Authors

Sofia Paiva